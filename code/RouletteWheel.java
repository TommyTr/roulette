import java.util.Random;
public class RouletteWheel {
    
    private Random random;
    private int num;

    public RouletteWheel(){
        this.random=new Random();
        this.num=0;
    }

    public void spin(){
        this.num=random.nextInt(37);
    }
    
    public int getValue(){
        return this.num;
    }

}
