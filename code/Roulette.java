import java.util.Scanner;
public class Roulette{

    public static void main(String[] args){
        
        RouletteWheel wheel=new RouletteWheel();
        Scanner scanner=new Scanner(System.in);
        int wallet=1000;
        System.out.println("Welcome to the roulette game! Best of luck as you start with $1000");
        System.out.println("Would you like to make a bet? y/n or yes/no");
        String answer=scanner.nextLine().toLowerCase();
        
        if(answer.equals("y") || answer.equals("yes")){

            System.out.println("How much do you wanna bet?");
            //"assuming the user enters a number of some sort"
            int amountBet=betValidation(scanner.nextInt(),wallet);

            System.out.println("What number would you like to bet on?");
            int num=numValidation(scanner.nextInt());

            wheel.spin();
            int wheelValue=wheel.getValue();
            System.out.println("The wheel value is "+wheelValue); 
            System.out.println("Your bet number is "+num);
            
            if(num==wheelValue){
                int winnings=amountBet*35;
                wallet+=winnings;
                System.out.println("And it aligns with your chosen bet number!");
                System.out.println("You have won "+winnings+" $! ("+amountBet+"$ * 35)");
            }else{
                System.out.println("You have lost your bet");
                wallet-=amountBet;
            }
            System.out.println("Your wallet now has "+wallet+"$");
            System.out.println("Thanks for playing!");
        }else{
            System.out.println("Have a nice day!");
        }
    }

    public static int betValidation(int amountBet,int wallet){
        Scanner scanner=new Scanner(System.in);
        while(amountBet>wallet || amountBet<0){
            System.out.println("Your bet can't be lower than 0 or greater than your wallet! Enter the amount again");
            amountBet=scanner.nextInt();
        }
        return amountBet;
    }
    public static int numValidation(int num){
        Scanner scanner=new Scanner(System.in);
        while(num<0 || num>36){
            System.out.println("Your number must be between 0 and 36! Enter a new number");
            num=scanner.nextInt();
        }
        return num;
    }
}